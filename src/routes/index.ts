// config server api
export const HOST_API = 'https://api.bettyscake.online/v1/'
export const HOST_API_INTERNAL = 'https://api.bettyscake.online/internal/v1/'

// page definition url in the app
export const PAGE_HOME = '/index'
export const PAGE_PRODUCTS = '/productos'
export const PAGE_FINISH_ORDER = `/finalizar-orden`
export const PAGE_CONFIRME_ORDER = `/confirmar-orden`
export const PAGE_COMPLETE_PROFILE = `/completar-info`

// Admin site
export const PAGE_ADMIN_HOME = '/managements'
export const PAGE_ADMIN_DETAILS_ORDER = '/managements/order'
export const PAGE_AUTH_LOGIN = `/auth/login`

// constants to api V1 definition
export const PRODUCTS = 'products'
export const MISCS = 'miscs'
export const ORDERS = 'orders'
export const AUTH = 'auth'
export const AUTH_LOGIN = `${AUTH}/login`
export const CHANGE_STATUS_ORDER = 'change-status'

// url to api definition
export const URL_POST_ORDER = 'URL_POST_ORDER'
export const URL_GET_LIST_MISCS = 'URL_GET_LIST_MISCS'
export const URL_GET_LIST_PRODUCTS = 'URL_GET_LIST_PRODUCTS'

// MANAGEMENT API
export const URL_AUTH_LOGIN = 'URL_AUTH_LOGIN'
export const URL_MANAGEMENT_UPDATE_ORDER = 'URL_MANAGEMENT_UPDATE_ORDER'
export const URL_MANAGEMENT_DELETE_ORDER = 'URL_MANAGEMENT_DELETE_ORDER'
export const URL_MANAGEMENT_LIST_ORDERS = 'URL_MANAGEMENT_LIST_PRODUCTS'
export const URL_MANAGEMENT_SEARCH_ORDER = 'URL_MANAGEMENT_SEARCH_ORDER'
export const URL_MANAGEMENT_GET_BY_ID_ORDER = 'URL_MANAGEMENT_GET_BY_ID_ORDER'
export const URL_MANAGEMENT_CHANGE_STATUS_ORDER = 'URL_MANAGEMENT_CHANGE_STATUS_ORDER'
