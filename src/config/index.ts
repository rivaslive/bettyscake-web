import axios from 'axios';
import { HOST_API, HOST_API_INTERNAL } from '../routes/';

export default axios.create({
	baseURL: HOST_API
});
export const axiosInternal = axios.create({
	baseURL: HOST_API_INTERNAL
});
export const MAX_PRODUCTS = 15
export const CELLPHONE_NUMBER = '2301-1310'