import React from "react";
import {Typography, Row, Col, Badge, Space, Button} from 'antd'
import styles from './styles.module.css'
import {ManagementOrder} from "../../models";
import moment from "moment";

const {Text} = Typography

export const getStatusColor = (status: string) => {
	switch (status) {
		case 'PENDING':
			return '#FF4D4F'
		case 'CONFIRMED':
			return '#FAAD14'
		case 'IN_PROCESS':
			return '#EB68CD'
		case 'COMPLETE':
			return '#52C41A'
		case 'ARCHIVED':
			return '#313945'
		default:
			return '#FF4D4F'
	}
}


interface InternalProps {
	order: ManagementOrder
	detailOrder: { (order_id: string): void }
}

const CardOrder: React.FC<InternalProps> = (props) => {
	const {order: {order_id, order_number, delivery_date, client_name, client_telephone, order_total, status, status_text}} = props

	// console.log(moment(delivery_date).format('D/M/Y'))
	return <div className={styles.cardOrder} onClick={() => props.detailOrder(order_id)}>
		<Row style={{marginBottom: 10}}>
			<Col span={11}>
				<Badge text={<span style={{color: `${getStatusColor(status)}`}}>{status_text}</span>} color={getStatusColor(status)}/>
			</Col>
			<Col span={13} style={{textAlign: 'right', width: '100%'}}>
				<Text><strong>N° {order_number}</strong></Text>
			</Col>
		</Row>
		<Space direction="vertical" style={{width: '100%'}}>
			<div style={{maxHeight: 17, overflow: 'hidden'}}>
				<Text>{client_name}</Text>
			</div>
			<div style={{maxHeight: 17, overflow: 'hidden'}}>
				<Text>Entrega: {moment(delivery_date).format('D/M/Y hh:mm a')}</Text>
			</div>
			<div style={{maxHeight: 13, overflow: 'hidden'}}>
				<Text>Tel.: {client_telephone}</Text>
			</div>
			<div style={{width: '100%'}}>
				<Row justify='center'>
					<Col span={8}>
						<div style={{marginTop: 5}}><Text style={{fontWeight: 'bold', fontSize: 'medium'}}>${order_total}</Text>
						</div>
					</Col>
					<Col span={16} style={{textAlign: 'right'}}>
						<Button style={{textAlign: 'right', marginTop: -5}} type='link'>Ver más detalles</Button>
					</Col>
				</Row>
			</div>
		</Space>
	</div>
}

export default CardOrder
