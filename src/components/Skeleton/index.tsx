import React, {useEffect, useState} from "react";
import {Row, Skeleton, Col} from 'antd';

interface InternalProps {
	number: number
}

const SkeletonProductList = (props: InternalProps) => {
	const [res, setRes] = useState([1, 2, 3, 4])
	useEffect(() => {
		const tempArray = []
		for (let i = 0; i < props.number; i++) {
			tempArray.push(i)
		}
		setRes(tempArray)
	}, [props.number])

	return <div>
		<Skeleton.Input active style={{width: 400, maxWidth: '100%', height: 25}}/>
		<div style={{marginTop: 30}}>
			<Row justify='center' gutter={[16, 16]}>
				{
					res.map((item, index) => {
						return <SkeletonProduct key={index}/>
					})
				}
			</Row>
		</div>
	</div>
}

const SkeletonProduct = () => {
	return <Col>
		<div style={{
			width: 175,
			height: 200,
		}}>
			<Skeleton.Button active style={{
				width: 175,
				height: 160
			}}/>

			<Skeleton.Button active style={{
				marginTop: 5,
				width: 175,
				height: 25
			}}/>
		</div>
	</Col>
}


export default SkeletonProductList
