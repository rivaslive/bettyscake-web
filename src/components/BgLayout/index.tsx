import React from "react";
import styles from './styles.module.css'
import {Link} from "react-router-dom";
import {PAGE_HOME} from "../../routes/";

interface InternalProps {
	children: any
	cancelTopBar?: boolean
}

const BgLayout: React.FC<InternalProps> = (props) => {
	const {children, cancelTopBar} = props
	return <section className={styles.sectionHome}>
		<div className="container-body" style={{position: 'absolute', marginTop: 20}}>
			<div style={{width: 80, height: 80}}>
				{!cancelTopBar && <Link to={PAGE_HOME}>
            <div
                style={{
									width: 80,
									height: 80,
									backgroundImage: `url("${process.env.PUBLIC_URL}/192x192.png")`,
									backgroundSize: 'cover'
								}}/>
        </Link>
				}
			</div>
		</div>
		<div className={`container-body ${styles.desplaceMarginTop}`}>
			<div className={styles.bodyHome}>
				{children}
			</div>
		</div>
	</section>
}

export default BgLayout
