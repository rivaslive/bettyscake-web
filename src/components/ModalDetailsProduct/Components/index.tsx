import {CELLPHONE_NUMBER, MAX_PRODUCTS} from "../../../config";
import React, {useEffect, useState} from "react";
import {MinusOutlined, PlusOutlined} from '@ant-design/icons'
import {Row, Col, Input, Space, Button, Tooltip, Typography, Alert} from "antd";
import {getStorage, verifyStorage} from "../../../services/Storage";

const {Text, Paragraph} = Typography

interface InternalProps {
	cant: number
	setCant: { (cant: number): void }
	closeModal: { (): void }
	dedications: string[]
	setDedications: { (dedications: string[]): void }
	onOk: { (): void }
	onChange: boolean
}

const FooterModal: React.FC<InternalProps> = (props) => {
	const {cant, setCant, closeModal, dedications, onOk, onChange} = props
	const [maxLength, setMaxLength] = useState(false)
	const [miscs, setMiscs] = useState<boolean>(false)

	useEffect(() => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const {countProduct, step} = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			setMiscs(step === 2)
			if (step) {
				return
			} else if (countProduct === MAX_PRODUCTS) {
				setMaxLength(true)
			}
		}
	}, [onChange])

	const handleCant = (action: string) => {
		if (action === 'prev' && cant > 1) {
			dedications.pop()
			return setCant(cant - 1)
		} else if (miscs) {
			dedications.push("")
			return setCant(cant + 1)
		} else if (action === 'next' && cant < MAX_PRODUCTS) {
			const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			if (verify) {
				const {countProduct} = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
				if (countProduct < MAX_PRODUCTS && (countProduct + cant) < MAX_PRODUCTS) {
					dedications.push("")
					return setCant(cant + 1)
				}
				return
			}
			dedications.push("")
			return setCant(cant + 1)
		}
	}

	return <Row>
		<Col span={12} style={{textAlign: 'left'}}>
			<Space size='small'>
				<Tooltip title="Menos">
					<Button
						onClick={() => handleCant('prev')}
						size="small"
						style={{background: '#8AB31E'}}
						shape="circle"
						icon={<MinusOutlined style={{paddingTop: 4, color: 'white'}}/>}/>
				</Tooltip>
				{/*Input*/}
				<Input
					placeholder='0'
					size="small"
					style={{width: 40, textAlign: 'center'}}
					value={maxLength ? '0' : cant}
					disabled/>
				<Tooltip title="Más">
					<Button
						onClick={() => handleCant('next')}
						size="small"
						style={{background: '#8AB31E'}}
						shape="circle"
						icon={<PlusOutlined style={{paddingTop: 4, color: 'white'}}/>}/>
				</Tooltip>
			</Space>
		</Col>
		<Col span={12} style={{textAlign: 'right'}}>
			<Space size='small'>
				<Button onClick={closeModal} type='link' style={{color: '#8AB31E'}}>Cerrar</Button>
				<Button type="primary" disabled={maxLength} onClick={onOk}>Añadir</Button>
			</Space>
		</Col>
	</Row>
}

interface DedicationInterface {
	cant: number
	dedications: string[]
	setDedications: any
	onChange: boolean
}

export const Dedications: React.FC<DedicationInterface> = (props) => {
	const {cant, dedications, onChange} = props
	const [maxLength, setMaxLength] = useState(false)


	useEffect(() => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const {countProduct, step} = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			if (step === 2) {
				return
			} else if (countProduct === MAX_PRODUCTS) {
				setMaxLength(true)
			}
		}
	}, [onChange])

	const handleInput = (event: any, i: number) => {
		dedications[i] = event.target.value
	}

	return <div style={{marginTop: 5}}>
		<Text style={{marginBottom: 0}}><strong>Dedicatorias({!maxLength ? cant : 0})</strong></Text>
		<br/>
		<Text style={{marginBottom: 0, fontSize: 'small'}}>Recuerda las dedicaciones son opcionales</Text>
		<Space direction="vertical" style={{width: '100%', marginTop: 7}}>
			{
				!maxLength && dedications.map((ded, i) => {
					return <Input
						key={i}
						onChange={event => handleInput(event, i)}
						style={{width: '100%'}}
						placeholder='Escribe la dedicatoria de tu pastel aquí'/>
				})
			}
			{
				maxLength && <Alert
            message="Ya has llegado al maximo de pedidos"
            description={<Text><Text>Por el momento solo podemos recibir cantidades no mayores a
							{MAX_PRODUCTS} productos, si crees que esto es un error puedes llamar al </Text> <Paragraph
							code>{CELLPHONE_NUMBER}</Paragraph></Text>}
            type="error"
        />
			}
		</Space>
	</div>
}

export default FooterModal
