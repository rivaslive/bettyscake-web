import {SelectPortions} from "..";
import React, {useEffect, useState} from "react";
import styles from './styles.module.css'
import {CloseOutlined} from '@ant-design/icons'
import {Carousel, Modal, Typography} from "antd";
import FooterModal, {Dedications} from "./Components";
import {Order, ShowModalAddProductInterface} from '../../models'
import {getStorage, initialOrder, setStorage, verifyStorage} from "../../services/Storage";

const {Title, Text} = Typography

interface InternalProps {
	step: number
	visible: boolean
	handleCancel: { (): void }
	product: ShowModalAddProductInterface
	triggerAddProduct: { (values: { cant: number, amount: number }): void }
}

const ModalDetailsProduct: React.FC<InternalProps> = (props) => {
	const [cant, setCant] = useState<number>(1)
	const [option_id, setOptionId] = useState("")
	const [dedications, setDedications] = useState([""])
	const {step, visible, handleCancel, product: {images, name, description, options, product_id}} = props

	useEffect(() => {
		setCant(1)
		setDedications([""])
	}, [visible])

	const addProduct = () => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			return addProductToLocalStorage()
		}
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, initialOrder)
		return addProductToLocalStorage()
	}

	const addProductToLocalStorage = () => {
		let amount: number = 0.1
		const recoverData: Order = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		console.log(recoverData);

		props.product.options.forEach((item) => {
			if (item.option_id === option_id) return amount = item.amount
		})
		recoverData.products = [...recoverData.products, {
			product_id,
			option_id,
			quantity: cant,
			dedications,
			amount,
			name,
			image: images.length ? images[0] : ""
		}]
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
		props.triggerAddProduct({amount, cant})
		return handleCancel()
	}

	return <Modal
		className={`${step === 2 && 'misc'} modal-no-header`}
		closeIcon={
			<div className={styles.close}>
				<CloseOutlined style={{fontSize: 10}}/>
			</div>
		}
		bodyStyle={{padding: 0}}
		visible={visible}
		onCancel={handleCancel}
		footer={
			<FooterModal
				cant={cant}
				setCant={setCant}
				closeModal={handleCancel}
				dedications={dedications}
				setDedications={setDedications}
				onOk={addProduct}
				onChange={visible}
			/>
		}
		destroyOnClose
		centered
	>

		{/*Carousel*/}
		<Carousel autoplay effect="fade">
			{
				images && images.map((img, i) => {
					return <div key={i}>
						<div className={styles.itemCarousel} style={{backgroundImage: `url('${img}')`}}/>
					</div>
				})
			}
		</Carousel>

		<div className={styles.body}>
			<div style={{lineHeight: '14px'}}>
				<Title level={3} style={{marginBottom: 0}}>{name}</Title>
				<Text>{description}</Text>
			</div>

			{/*Selection of portions*/}
			<SelectPortions options={options} setOptionId={setOptionId}/>

			{/*Dedicatoria*/}
			{
				step !== 2 && <Dedications onChange={visible} cant={cant} dedications={dedications} setDedications={setDedications}/>
			}

		</div>

	</Modal>
}

export default ModalDetailsProduct
