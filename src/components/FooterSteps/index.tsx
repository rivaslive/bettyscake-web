import React from "react";
import styles from "./styles.module.css";
import {OrderProduct} from "../../models";
import {ShoppingCartOutlined, DeleteOutlined} from '@ant-design/icons'
import {Button, Col, Row, Space, Typography, Dropdown, Menu, Badge} from "antd";
import {getStorage, setStorage, verifyStorage} from "../../services/Storage";
const {Text} = Typography

interface InternalProps {
	refresh: { (): void }
	back?: any
	backText?: string
	okText?: string
	productsCard: OrderProduct[]
	countCant: number
	countProduct: number
	total: number
	functionNext: any
	resetOrder: { (): void }
	btnLoading?: boolean
	history: any
}

const FooterSteps: React.FC<InternalProps> = (props) => {
	const {countCant, countProduct, total, functionNext, resetOrder, productsCard, btnLoading, okText, back} = props

	const deleteProduct = (i: number) => {
		if (verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)) {
			const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			recoverData.countProduct = recoverData.countProduct - recoverData.products[i].quantity
			recoverData.total = recoverData.total - (recoverData.products[i].amount * recoverData.products[i].quantity)
			recoverData.products.splice(i, 1)
			setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
			if (recoverData.products.length === 0 ) {
				recoverData.step = 1
				setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
			}
			return props.refresh()
		}
	}

	const menu = (
		<Menu>
			{
				productsCard.map((item, i) => {
					return <Menu.Item key={i}>
						<div
							className={`${styles.cardShopping} ${(productsCard.length - 1) !== i ? styles.bottomLine : ''}`}>
							<div>
								<img src={item.image} alt={item.name} width={85} style={{borderRadius: 6}}/>
							</div>
							<div style={{paddingLeft: 10, width: '100%'}}>
								<Text><strong>{item.name}</strong></Text>
								<br/>
								<Text>Cantidad: {item.quantity} sub-total: ${item.quantity * item.amount}</Text>
								<br/>
								<div style={{textAlign: 'right'}}>
									<Button onClick={() => deleteProduct(i)} title='eliminar' type='primary' icon={<DeleteOutlined/>}
													shape='circle' danger size='small'/>
								</div>
							</div>
						</div>
					</Menu.Item>
				})
			}
		</Menu>
	);
	return <React.Fragment>
		<div style={{position: 'fixed', top: 35, left: 15, zIndex: 800}}>
			{back}
		</div>

		<div className={styles.footer}>
			<div className={`container-body ${styles.footerActions}`}>
				<Dropdown overlay={menu} trigger={['click']}>
					<Button className={`btn-green ${styles.footerDetails}`}>
						<Row>
							<Col span={2}>
								<Badge count={countProduct} style={{background: '#EB68CD'}}>
									<ShoppingCartOutlined style={{fontSize: 25}}/>
								</Badge>
							</Col>
							<Col span={12} style={{textAlign: 'left', overflow: 'hidden'}}>
								Cantidad de productos: {countCant}
							</Col>
							<Col span={10} style={{textAlign: 'right', overflow: 'hidden'}}>
								Total de la orden: ${total}
							</Col>
						</Row>
					</Button>
				</Dropdown>
				<Button onClick={resetOrder} className={styles.footerReset}>Reiniciar compra</Button>
				<Button loading={btnLoading} type='primary' onClick={functionNext}
								className={styles.footerNext}>{okText !== undefined ? okText : 'Continuar'}</Button>
			</div>
		</div>

		<Dropdown overlay={menu} trigger={['click']}>
			<div className={styles.headerDetails}>
				<Space direction='horizontal'>
					<div style={{paddingRight: 5}}>
						<div>
							<Text className='text-white text-bold'>Cantidad: {countCant}</Text>
						</div>
						<div>
							<Text className='text-white text-bold'>Total: ${total}</Text>
						</div>
					</div>
					<div>
						<div className={styles.leftLine}>
							<Badge count={countProduct} style={{background: '#EB68CD'}}>
								<ShoppingCartOutlined style={{fontSize: 30}}/>
							</Badge>
						</div>
					</div>
				</Space>
			</div>
		</Dropdown>
	</React.Fragment>
}

export default FooterSteps
