import React, {useEffect, useState} from "react";
import styles from './styles.module.css'
import {Button, Col, Row, Typography} from "antd";

const {Text} = Typography

interface InternalProps {
	options: {
		option_id: string
		name: string
		amount: number
	}[]
	setOptionId: { (id: string): void }
}

const SelectPortions: React.FC<InternalProps> = (props) => {
	const [selected, setSelected] = useState(0)

	useEffect(()=> {
		props.setOptionId(props.options.length ? props.options[0].option_id : '')
		// eslint-disable-next-line
	}, [props.options])

	const handleOk = (i: number, option_id: string) => {
		props.setOptionId(option_id)
		return setSelected(i)
	}

	return <div style={{marginTop: 5}}>
		<Text><strong>Selecciona porciones y precios</strong></Text>
		<div style={{marginTop: 7}}>
			<Row justify='start' gutter={[8, 8]}>
				{
					props.options.map((opt, i) => {
						return <Col key={i}>
							<Button
								onClick={() => handleOk(i, opt.option_id)}
								type="primary"
								className={`${styles.btnSelect} ${selected === i && styles.onSelected}`}>
								<Text><strong>${opt.amount}</strong></Text>
								<br/>
								<Text style={{fontSize: 'x-small'}}>{opt.name}</Text>
							</Button>
						</Col>
					})
				}
			</Row>
		</div>
	</div>
}

export default SelectPortions
