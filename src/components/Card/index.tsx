import React from "react";
import {Typography} from "antd";
import styles from "./styles.module.css"
import {ShowModalAddProductInterface} from "../../models";

const {Text} = Typography

interface InternalProps {
	product_id: string
	images: string[]
	options: {
		option_id: string
		name: string
		amount: number
	}[]
	name: string
	price: {
		min: string
		max: string
		range: string
	}
	isPremium: boolean
	addProduct: {({images, name, description, options}: ShowModalAddProductInterface):void}
	description: string
}

const CardDefault: React.FC<InternalProps> = (props) => {
	const {images, options, isPremium, name, price, addProduct, description, product_id} = props

	return <div className={styles.card} onClick={() => addProduct({name, images, description, options, product_id})}>
		<div className={styles.cardImage}
				 style={{backgroundImage: `url("${images.length ? images[0] : 'https://scontent.fymy1-1.fna.fbcdn.net/v/t1.0-9/p720x720/96860398_3062966037059184_6852410374176636928_n.jpg?_nc_cat=102&_nc_sid=2d5d41&_nc_ohc=lREL2D25aaAAX-dzSxP&_nc_ht=scontent.fymy1-1.fna&_nc_tp=6&oh=00eb7b048606cf63916ea8141da8ac10&oe=5EF3A434'}")`}}>
			{
				!isPremium && <div className={styles.cardAmount}>
            <Text style={{fontSize: 18, color: 'white', fontWeight: 'bold'}}>${options[0].amount}</Text>
        </div>
			}
		</div>
		<div className={styles.cardText}>
			{
				!isPremium ?
					<Text>{options[0].name}</Text>
					:
					<div>
						<Text><strong>{name}</strong></Text>
						<br/>
						<Text style={{fontSize: 12}}>{price.range}</Text>
					</div>
			}
		</div>
	</div>
}

export default CardDefault