import emoji from 'react-easy-emoji'

export default function Emoji(input: string) {
	return emoji(input, {
		baseUrl: 'https://twemoji.maxcdn.com/2/svg/',
		ext: '.svg',
		size: ''
	})
}