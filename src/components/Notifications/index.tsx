import {notification} from 'antd'

interface InternalProps {
	type: 'success' | 'error' | 'info' | 'warning' | 'warn' | 'open'
	message: string
	description: string
	icon?: any
}

const sendNotification = (props: InternalProps) => {
	const {type, message, description, icon} = props
	return notification[type]({
		message,
		description,
		...icon
	});
}

export default sendNotification