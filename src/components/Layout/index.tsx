import React from "react";
import {Layout} from "antd";
import {PAGE_AUTH_LOGIN, PAGE_FINISH_ORDER, PAGE_HOME} from '../../routes/'
import {Link} from 'react-router-dom'

const {Header, Content} = Layout;

const LayoutComponent: React.FC<any> = (props) => {

	return <Layout>
		{
			window.location.pathname !== PAGE_HOME && window.location.pathname !== PAGE_FINISH_ORDER && window.location.pathname !== PAGE_AUTH_LOGIN &&
      <Header style={{
				position: 'fixed',
				top: 0,
				left: 0,
				backgroundColor: "white",
				height: 100,
				width: '100%',
				paddingTop: 10,
				paddingLeft: 0,
				paddingRight: 0,
				zIndex: 800
			}}>
          <div className={`container-body header-responsive`}>
              <div style={{maxWidth: 80}}>
                  <Link to={PAGE_HOME}>
                      <div
												className="header-img-responsive"
                          style={{
														width: 80,
														height: 80,
														backgroundImage: `url("${process.env.PUBLIC_URL}/192x192.png")`,
														backgroundSize: 'cover'
													}}/>
                  </Link>
              </div>
          </div>
      </Header>
		}
		<Content style={{marginTop: 100}}>
			{props.children}
		</Content>
	</Layout>
}

export default LayoutComponent