import React, {useEffect, useState} from "react";
import {Link, withRouter} from "react-router-dom";
import {PAGE_ADMIN_HOME, PAGE_PRODUCTS} from "../../routes/";
import {Emoji, BgLayout} from '../../components';
import {Typography, Timeline, Button} from "antd";
import {delStorage, verifyStorage} from "../../services/Storage";

const {Title, Text} = Typography;

const HomePage: React.FC<any> = (props) => {

	const [newOrder, setNewOrder] = useState(false)
	useEffect(() => {
		verifyOrder()
	}, [])

	const verifyOrder = () => {

		if (verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)) return setNewOrder(true)
	}
	const resetOrder = () => {

		delStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		props.history.push(PAGE_PRODUCTS)
	}
	return <BgLayout>
		<div>
			<Title level={3}>¡El sabor de Betty’s Cake hasta la puerta de tu casa! {Emoji('🥳')}</Title>
			{
				newOrder ?
					<div style={{marginBottom: 20}}>
						<Text>{Emoji('🧐')} Parece que tienes una orden en proceso:</Text>
					</div>
					:
					<React.Fragment>
						<Text>{Emoji('🧐')} Lo que debes tener en cuenta antes de utilizar nuestra web:</Text>

						<Timeline style={{marginTop: 20, marginBlockEnd: 0}}>
							{
								data.map((item, index) => {
									return <Timeline.Item
										dot={<Text>{Emoji('👉')}</Text>}
										key={index}
										color="green">
										{item.text} {Emoji(item.emoji)}
									</Timeline.Item>
								})
							}
						</Timeline>
					</React.Fragment>
			}

			<Link to={PAGE_PRODUCTS}>
				<Button
					type='primary'
					style={{width: '95%', marginTop: -30, marginBottom: 20}}>{newOrder ? 'SEGUIR COMPRANDO' : 'COMENZAR'}</Button>
			</Link>
			{
				newOrder && <Button
            onClick={resetOrder}
            type='default'
            style={{width: '95%', marginTop: -20, background: 'rgb(138, 179, 30)', color: 'white'}}>
            EMPEZAR DE NUEVO</Button>
			}
			<div style={{width: '100%', textAlign: 'center', marginTop: -20, marginBottom: 50}}><Link
				to={PAGE_ADMIN_HOME}><strong>¿Eres un administrador?</strong></Link></div>
		</div>
	</BgLayout>

}


const data = [
	{text: "La información personal que nos brindes será un secreto nuestro ", emoji: '🤫'},
	{text: "¡Esta web te servirá para que puedas escoger tu Pastel ideal en tres sencillos pasos!", emoji: "😉"},
	{text: "No necesitarás una tarjeta de crédito o débito para poder crear una orden. ¿No es grandioso?", emoji: "🤯"},
	{text: "Necesitarás un número de teléfono que sea real, porque te llamaremos.", emoji: '💪🏻'},
	{text: "¡Podrás elegir entre variedades de Cakes y extras como misceláneos!", emoji: '🤩'},
]

export default withRouter(HomePage);
