import React, {useEffect, useState} from "react"
import {withRouter} from "react-router-dom";
import {LoadingOutlined} from '@ant-design/icons'
import {
	Descriptions,
	PageHeader,
	Collapse,
	Typography,
	Table,
	Timeline,
	Button,
	Popconfirm,
	Row,
	Col, Result, Badge, Modal, Form, Input, DatePicker, InputNumber
} from "antd";
import moment from "moment";
import {serverRequest} from "../../services";
import {
	PAGE_ADMIN_HOME,
	URL_MANAGEMENT_CHANGE_STATUS_ORDER,
	URL_MANAGEMENT_DELETE_ORDER,
	URL_MANAGEMENT_GET_BY_ID_ORDER, URL_MANAGEMENT_UPDATE_ORDER
} from "../../routes/";
import {ManagementOrder} from "../../models";
import sendNotification from "../../components/Notifications";
import {getStatusColor} from "../../components/CardOrder";

const {Text, Title} = Typography
const {Panel} = Collapse;

const DetailsManagement: React.FC<any> = (props) => {
	const [form] = Form.useForm()
	const [notFound, setNotFound] = useState(false)
	const [visible, setVisible] = useState(false)
	const [loading, setLoading] = useState(true)
	const [btnLoading, setBtnLoading] = useState(false)
	const [order, setOrder] = useState<ManagementOrder>(initialOrder)

	useEffect(() => {
		getData().catch(() => {
		})
		// eslint-disable-next-line
	}, [])

	const getData = async () => {
		const {order_id} = props.match.params
		const data = await serverRequest({action: URL_MANAGEMENT_GET_BY_ID_ORDER, id: order_id})
		setLoading(false)
		if (data) {
			setOrder(data)
			// const newNumber = parseInt(data.client_telephone, 10)
			// data.client_telephone = isNaN(newNumber) ? 0 : newNumber
			data.delivery_date = moment(data.delivery_date)
			return form.setFieldsValue(data)
		}
		return setNotFound(true)
	}

	const changeStatus = async (status: string) => {
		setBtnLoading(true)
		const {order_id} = order
		const data = await serverRequest({
			action: URL_MANAGEMENT_CHANGE_STATUS_ORDER,
			body: {order_id, status}
		})
		setBtnLoading(false)
		if (data) {
			return getData()
		}
		return sendNotification({message: 'Alerta', type: 'warning', description: 'No se pudo eliminar la orden'})
	}

	const deleteOrder = async () => {
		setBtnLoading(true)
		const {order_id} = order
		const data = await serverRequest({action: URL_MANAGEMENT_DELETE_ORDER, id: order_id})
		setBtnLoading(false)
		if (data) return props.history.push(PAGE_ADMIN_HOME)
		return sendNotification({message: 'Alerta', type: 'warning', description: 'No se pudo eliminar la orden'})
	}

	const handleOk = async () => {
		const body = await form.validateFields()
		console.log(body);
		if (body.errorFields === undefined) {
			const data = await serverRequest({action: URL_MANAGEMENT_UPDATE_ORDER, body, id: order.order_id})
			setVisible(false)
			if (data) {
				return getData()
			}
			return sendNotification({type:'error', message: 'Alerta', description: 'Error al editar la orden'})
		}
	}

	const handleCancel = () => {
		return setVisible(false)
	}

	const onFinish = () => {

	}

	function disabledDate(current: any) {
		return current && current < moment(new Date()).add(-1, 'days');
	}

	return <React.Fragment>
		{
			notFound ?
				<Result
					status="404"
					title="No encontrado"
					subTitle="No hemos podido encontrar esta orden, lo sentimos."
					extra={<Button type="primary" onClick={() => props.history.push(PAGE_ADMIN_HOME)}>Regresar</Button>}
				/> :
				<div>
					<PageHeader
						className="site-page-header"
						onBack={() => props.history.push(PAGE_ADMIN_HOME)}
						title="Orden"
						subTitle="Kevin Rivas"
						extra={<img src={`${process.env.PUBLIC_URL}/192x192.png`} width={75} alt="Logo"/>}
					/>
					{
						loading ?
							<div style={{width: '100%', textAlign: 'center', marginTop: 50}}>
								<LoadingOutlined style={{fontSize: 40, color: '#EB68CD'}}/>
							</div>
							:
							<div className='container-body'>
								<div style={{marginTop: 20}}>

									<div style={{width: '100%', textAlign: 'center', marginBottom: 20}}>
										<Badge
											className='extra-badge'
											text={<span style={{color: `${getStatusColor(order.status)}`}}>{order.status_text}</span>}
											color={getStatusColor(order.status)}/>
									</div>

									<Descriptions title="Información de la orden" layout="horizontal" column={2}>
										<Descriptions.Item label={<strong>Orden</strong>}>{order.order_id}</Descriptions.Item>

										<Descriptions.Item label={<strong>Numero de orden</strong>}>
											{order.order_number}
										</Descriptions.Item>

										<Descriptions.Item label={<strong>Nombre</strong>}>
											{order.client_name}
										</Descriptions.Item>

										<Descriptions.Item label={<strong>Teléfono</strong>}>
											{order.client_telephone}
										</Descriptions.Item>

										<Descriptions.Item label={<strong>Fecha y hora de entrega</strong>}>
											<Text>{moment(order.delivery_date).format("dddd D, MMMM YYYY, h:mm a")}</Text>
										</Descriptions.Item>

										<Descriptions.Item label={<strong>Dirección</strong>}>
											{order.client_direction}
										</Descriptions.Item>

										<Descriptions.Item label={<strong>Comentario adicional</strong>}>
											<Text>{order.client_comment ? order.client_comment : 'Ninguno'}</Text>
										</Descriptions.Item>

										<Descriptions.Item label={<strong>SubTotal</strong>}>
											<Text>${order.order_total}</Text>
										</Descriptions.Item>

										<Descriptions.Item label={<strong>Envío</strong>}>
											<Text>${order.cost_delivery}</Text>
										</Descriptions.Item>

										<Descriptions.Item label={<strong style={{fontSize: 'medium'}}>TOTAL A PAGAR</strong>}>
											<Text style={{fontSize: 'medium'}}>${order.order_total + order.cost_delivery}</Text>
										</Descriptions.Item>
									</Descriptions>

									<br/>

									<Row justify={'end'} gutter={[16, 16]}>
										{
											order.status !== "CONFIRMED" && order.status !== "IN_PROCESS" && order.status !== "ARCHIVED" &&
                      <Col>
                          <Popconfirm
                              onConfirm={() => changeStatus('CONFIRMED')}
                              title="¿Esta seguro de confirmar la orden para poder ser procesada?" okText="Si"
                              cancelText="No"
                          >
                              <Button loading={btnLoading} style={{background: '#FAAD14', color: 'white'}}>Confirmar
                                  pedido</Button>
                          </Popconfirm>
                      </Col>
										}
										{
											order.status !== "IN_PROCESS" && order.status !== "PENDING" && order.status !== "ARCHIVED" &&
                      <Col>
                          <Popconfirm onConfirm={() => changeStatus('IN_PROCESS')}
                                      title="¿Esta seguro de empezar hacer la orden?"
                                      okText="Si" cancelText="No">
                              <Button loading={btnLoading} style={{background: '#EB68CD', color: 'white'}}>Hacer la
                                  orden</Button>
                          </Popconfirm>
                      </Col>
										}
										{
											order.status !== "ARCHIVED" && order.status !== "CONFIRMED" && order.status !== "PENDING" &&
                      <Col>
                          <Popconfirm onConfirm={() => changeStatus('ARCHIVED')} title="¿La orden ya fue entregada?"
                                      okText="Si"
                                      cancelText="No">
                              <Button loading={btnLoading}
                                      style={{background: '#313945', color: 'white'}}>Finalizar</Button>
                          </Popconfirm>
                      </Col>
										}
										<Col>
											<Button onClick={() => setVisible(true)} loading={btnLoading} type={"dashed"}
															style={{borderColor: '#EB68CD', color: '#EB68CD'}}>
												Editar orden</Button>
										</Col>
										<Col>
											<Popconfirm
												onConfirm={deleteOrder}
												title="¿Esta seguro de eliminar esta orden, no se podrá recuperar?" okText="Si"
												cancelText="No">
												<Button loading={btnLoading} danger>Eliminar orden</Button>
											</Popconfirm>
										</Col>
									</Row>

									<Title level={3}>Productos</Title>
									<Collapse defaultActiveKey={['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']}>
										{
											order.products.map((product, i) => {
												return <Panel key={i} header={<strong
													style={{color: '#EB68CD'}}>{`${product.quantity} ${product.name}`}</strong>}>
													<Table
														columns={columns}
														dataSource={[product]}
														pagination={false}
													/>
													<div style={{marginTop: 15}}>
														<Text style={{fontWeight: 'bold', fontSize: 'medium'}}>Dedicatorias</Text>
														<Timeline style={{marginTop: 15}}>
															{
																product.dedications.map((ded, i) => {
																	if (ded) {
																		return <Timeline.Item key={i}>{ded}</Timeline.Item>
																	} else return false
																})
															}
														</Timeline>
													</div>
												</Panel>
											})
										}
									</Collapse>
									<br/>
									<br/>
									<br/>
								</div>
							</div>
					}
				</div>
		}

		<Modal
			title="Editar información del cliente"
			visible={visible}
			onOk={handleOk}
			onCancel={handleCancel}
		>
			<Form form={form} name="edit-order-form" onFinish={onFinish}>
				<Form.Item rules={[{required: true, message: 'Campo obligatorio'}]} labelCol={{span: 24, offset: 24}}
									 labelAlign='left' name="client_name" label="Nombre del cliente">
					<Input placeholder="Ejemplo: Carlos Vides Guardado"/>
				</Form.Item>

				<Form.Item rules={[{required: true, message: 'Campo obligatorio'}]} labelCol={{span: 24, offset: 24}}
									 labelAlign='left' name="client_telephone" label="Teléfono">
					<InputNumber style={{width: '100%'}} placeholder="Ejemplo: 23010045"/>
				</Form.Item>

				<Form.Item rules={[{required: true, message: 'Campo obligatorio'}]} labelCol={{span: 24, offset: 24}}
									 labelAlign='left' name="client_direction" label="Dirección">
					<Input.TextArea placeholder="Frente a parque central, casa #56"/>
				</Form.Item>

				<Form.Item rules={[{required: true, message: 'Campo obligatorio'}]} labelCol={{span: 24, offset: 24}}
									 labelAlign='left' name="delivery_date" label="Fecha y hora de entrega">
					<DatePicker
						style={{width: '100%'}}
						format="YYYY-MM-DD HH:mm"
						disabledDate={disabledDate}
						showTime={{defaultValue: moment('00:00:00', 'HH:mm:ss')}}
					/>
				</Form.Item>

				<Form.Item rules={[{required: true, message: 'Campo obligatorio'}]} labelCol={{span: 24, offset: 24}}
									 labelAlign='left' name="client_email" label="Correo electrónico">
					<Input placeholder="Ejemplo: exmaple@mail.com"/>
				</Form.Item>

				<Form.Item rules={[{required: true, message: 'Campo obligatorio'}]} labelCol={{span: 24, offset: 24}}
									 labelAlign='left' name="cost_delivery" label="Costo de envío">
					<InputNumber style={{width: '100%'}} placeholder="Ejemplo: 3.00"/>
				</Form.Item>

			</Form>
		</Modal>
	</React.Fragment>
}

const initialOrder = {
	order_id: '',
	order_number: 0,
	delivery_date: '',
	delivery_date_text: '',
	client_name: '',
	client_direction: '',
	client_telephone: '',
	cost_delivery: 0,
	products: [],
	client_comment: '',
	client_email: '',
	order_total: 0,
	payment: 0,
	remaning_payment: 0,
	status: '',
	status_text: '',
}

const columns = [
	{
		title: 'Cantidad',
		dataIndex: 'quantity',
		key: 'quantity'
	},
	{
		title: 'Producto',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'Monto',
		dataIndex: 'amount',
		key: 'amount',
		render: (text: any) => <Text key='2'>${text}</Text>
	},
	{
		title: 'Precio total',
		dataIndex: 'amount',
		key: 'total',
		render: (text: any, row: any) => <Text key='1'>${text * row.quantity}</Text>
	}
]


export default withRouter(DetailsManagement)
