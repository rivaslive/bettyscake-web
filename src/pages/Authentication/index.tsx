import React, {useState} from 'react'
// import styles from './style.module.css'
import {withRouter} from 'react-router-dom'
import {BgLayout} from "../../components";
import {Typography, Form, Input, Button, Alert} from 'antd'
import {serverRequest} from "../../services";
import {PAGE_ADMIN_HOME, URL_AUTH_LOGIN} from "../../routes/";
import {setStorage} from "../../services/Storage";

const {Title} = Typography

const AuthPage: React.FC<any> = (props) => {
	// const [form] = Form.useForm()
	const [loginError, setLoginError] = useState("")
	const [btnLogin, setBtnLogin] = useState(false)

	const onFinish = async (body: any) => {
		setBtnLogin(true)
		const data = await serverRequest({action: URL_AUTH_LOGIN, body})
		if (data) {
			setStorage('auth', data)
			return props.history.push(PAGE_ADMIN_HOME)
		}
		setLoginError("Usuario o contraseña incorrecto")
		setBtnLogin(false)
	}

	return <BgLayout>
		<Title level={3}>Inicia sesión</Title>
		<div>
			{loginError && <div style={{marginBottom: 20}}><Alert type='error' message={loginError} closable/></div>}
			<Form
				className='form-resuelve-padding'
				name="login"
				initialValues={{remember: true}}
				onFinish={onFinish}
			>
				<Form.Item
					label="Nombre de usuario"
					name="user_name"
					labelCol={{span: 24, offset: 24}} labelAlign='left'
					rules={[{required: true, message: 'ingresa tu username'}]}
				>
					<Input/>
				</Form.Item>

				<Form.Item
					label="Contraseña"
					name="password"
					labelCol={{span: 24, offset: 24}} labelAlign='left'
					rules={[{required: true, message: 'Please input your password!'}]}
				>
					<Input.Password/>
				</Form.Item>

				<Form.Item style={{marginTop: 40}}>
					<Button loading={btnLogin} style={{width: '100%'}} type="primary" htmlType="submit">
						Iniciar sesión
					</Button>
				</Form.Item>
			</Form>
		</div>
	</BgLayout>
}

export default withRouter(AuthPage)
