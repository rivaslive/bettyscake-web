import * as React from "react";
import {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import {serverRequest} from "../../../services";
import {Button, Col, message, Row, Typography} from "antd";
import {ShowModalAddProductInterface} from '../../../models';
import sendNotification from "../../../components/Notifications";
import {DeleteOutlined, ArrowLeftOutlined} from '@ant-design/icons';
import {delStorage, getStorage, setStorage, verifyStorage} from "../../../services/Storage";
import {CardDefault, FooterSteps, SkeletonProductsList, ModalDetailsProduct} from '../../../components'
import {
	PAGE_COMPLETE_PROFILE,
	PAGE_CONFIRME_ORDER,
	PAGE_FINISH_ORDER, PAGE_HOME,
	PAGE_PRODUCTS,
	URL_GET_LIST_MISCS,
	URL_GET_LIST_PRODUCTS
} from "../../../routes/";

const {Title} = Typography

const Products: React.FC<any> = (props) => {
	const [step, setStep] = useState<any>(1)
	const [visible, setVisible] = useState<any>(false)
	const [addProduct, setAddProduct] = useState<any>({name: '', description: '', images: [], options: []})
	const [products, setProducts] = useState<any>([])
	const [productsCard, setProductsCard] = useState<any>([])
	const [orderCount, setOrderCount] = useState({total: 0, countProduct: 0})
	const [loading, setLoading] = useState(true)

	const getProducts = async () => {
		const data = await serverRequest({action: URL_GET_LIST_PRODUCTS})
		data && setProducts(data.items)
	}
	const getMiscs = async () => {
		const data = await serverRequest({action: URL_GET_LIST_MISCS})
		data && setProducts(data.items)
	}


	useEffect(() => {
		verifyState().catch(() => {
		})
		// eslint-disable-next-line
	}, [])

	const verifyState = async () => {
		setLoading(true)
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const data = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			setOrderCount({total: data.total, countProduct: data.countProduct})
			setProductsCard(data.products)
			if (data.step === 1) {
				await getProducts()
			} else if (data.step === 2) {
				await getMiscs()
			} else if (data.step === 3) {
				return props.history.push(PAGE_COMPLETE_PROFILE)
			} else if (data.step === 4) {
				return props.history.push(PAGE_CONFIRME_ORDER)
			} else if (data.step === 5) {
				return props.history.push(PAGE_FINISH_ORDER)
			} else {
				props.history.push(`${PAGE_PRODUCTS}/1`)
				await getProducts()
			}
			setStep(data.step)
			setLoading(false)
			return
		} else {
			await getProducts()
			setOrderCount({total: 0, countProduct: 0})
			setStep(1)
		}
		return setLoading(false)
	}

	// Modal Actions
	const showModal = (prod: ShowModalAddProductInterface) => {
		setAddProduct(prod)
		setVisible(true)
	}
	const closeModal = () => {
		setVisible(false)
	}

	const triggerAddProduct = (values: { cant: number, amount: number }) => {
		const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		const total = recoverData.total + (values.amount * values.cant)
		const cant = recoverData.countProduct + values.cant

		recoverData.total = total
		recoverData.countProduct = cant

		setProductsCard(recoverData.products)
		setOrderCount({total, countProduct: cant})

		return setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
	}

	const resetOrder = async () => {
		const del = delStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		del && sendNotification({
			description: 'Se a vaciado tu orden',
			message: 'Notification',
			type: 'open',
			icon: {icon: <DeleteOutlined style={{color: 'orangered'}}/>}
		})
		setProductsCard([])
		return verifyState()
	}

	const nextStep = async () => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			if (step === 2) {
				recoverData.step = 3
				setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
				return props.history.push(PAGE_COMPLETE_PROFILE)
			}
			if (recoverData.products.length) {
				recoverData.step = 2
				setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
				await verifyState()
				return props.history.push(`${PAGE_PRODUCTS}/2`)
			}
		}
		return message.warn('Añade un producto antes de continuar')
	}

	const back = () => {
		if (step === 1) {
			return props.history.push(PAGE_HOME)
		} else {
			const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			recoverData.step = 1
			setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
			props.history.push(`${PAGE_PRODUCTS}/1`)
			return verifyState()
		}
	}

	return <main style={{marginTop: 25, marginBottom: 25}}>
		<div className='container-body' style={{marginBottom: 75}}>
			{
				loading ?
					// Skeleton
					<React.Fragment>
						<SkeletonProductsList number={4}/>
						<SkeletonProductsList number={5}/>
					</React.Fragment>
					:
					// List products
					<React.Fragment>
						{
							step === 2 ?
								<section>
									<Title level={3}>Agregar extras(opcional)</Title>
									<div style={{marginTop: 30}}>
										<Row gutter={[16, 16]} justify='center'>
											{
												products.length > 0 && products.map((item: any, i: number) => {
													return <Col key={i}>
														<CardDefault
															product_id={item.product_id}
															name={item.name}
															description={item.description}
															images={item.images}
															price={item.price}
															options={item.options}
															addProduct={showModal}
															isPremium={true}
														/>
													</Col>
												})
											}
										</Row>
									</div>
								</section>
								:
								products.length > 0 && products.map((cat: any, index: number) => {
									return <section key={index}>
										<Title level={3}>{cat.name}</Title>
										<div style={{marginTop: 30}}>
											<Row gutter={[16, 16]} justify='center'>
												{
													cat.products.map((item: any, i: number) => {
														return <Col key={i}>
															<CardDefault
																product_id={item.product_id}
																name={item.name}
																description={item.description}
																images={item.images}
																price={item.price}
																options={item.options}
																addProduct={showModal}
																isPremium={item.offer_type !== 'regular'}
															/>
														</Col>
													})
												}
											</Row>
										</div>
									</section>
								})
						}
					</React.Fragment>
			}
		</div>

		{/*Footer Buttons*/}
		<FooterSteps
			history={props.history}
			refresh={verifyState}
			back={<Button onClick={back} type='link' size='large' icon={<ArrowLeftOutlined/>}>{props.backText ? props.backText : 'Atras'}</Button>}
			productsCard={productsCard}
			countCant={orderCount.countProduct}
			countProduct={productsCard.length !== 0 ? productsCard.length : 0}
			total={orderCount.total}
			resetOrder={resetOrder}
			functionNext={nextStep}
		/>

		{/*Modal add*/}
		<ModalDetailsProduct
			step={step}
			product={addProduct}
			handleCancel={closeModal}
			visible={visible}
			triggerAddProduct={triggerAddProduct}
		/>

	</main>
}

export default withRouter(Products)