import {Order} from "../../../models";
import styles from './styles.module.css'
import {withRouter} from 'react-router-dom'
import {serverRequest} from "../../../services";
import {FooterSteps} from '../../../components';
import React, {useEffect, useState} from 'react';
import {CELLPHONE_NUMBER} from "../../../config";
import {Descriptions, Typography, Table, Button, Alert} from 'antd'
import sendNotification from "../../../components/Notifications";
import {DeleteOutlined, ArrowLeftOutlined} from '@ant-design/icons';
import {PAGE_COMPLETE_PROFILE, PAGE_FINISH_ORDER, PAGE_PRODUCTS, URL_POST_ORDER} from "../../../routes/";
import {delStorage, getStorage, setStorage, verifyStorage} from "../../../services/Storage";
import moment from "moment";

const {Title, Text} = Typography

const ConfirmeOrder: React.FC<any> = (props) => {
	const [order, setOrder] = useState<Order>({
		order_id: "",
		order_number: 0,
		delivery_date: "",
		client_name: "",
		client_direction: "",
		client_telephone: "",
		products: [],
		client_comment: "",
		client_email: "",
		total: 0,
		countProduct: 0,
		step: 4
	})
	const [btnLoading, setBtnLoading] = useState(false)

	useEffect(() => {
		getState()
		// eslint-disable-next-line
	}, [])

	const getState = () => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const data = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			console.log(data);
			if (data.step === 4) {
				return setOrder(data)
			} else {
				return props.history.push(PAGE_PRODUCTS)
			}
		}
		return props.history.push(PAGE_PRODUCTS)
	}

	const onFinish = async () => {
		setBtnLoading(true)
		const data: any = await serverRequest({action: URL_POST_ORDER, body: order})
		console.log(data);
		if (data) {
			const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			recoverData.step = 5
			recoverData.order_id = data.order_id
			recoverData.order_number = data.order_number
			setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
			setBtnLoading(false)
			return props.history.push(PAGE_FINISH_ORDER)
		}
		setBtnLoading(false)
		return sendNotification({
			description: `Tu pedido no fue realizado, intenta mas tarde o llama al ${CELLPHONE_NUMBER} para mas información`,
			message: 'La orden falló',
			type: 'warning'
		})
	}

	const resetOrder = async () => {
		const del = delStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		del && sendNotification({
			description: 'Se a vaciado tu orden',
			message: 'Notification',
			type: 'open',
			icon: {icon: <DeleteOutlined style={{color: 'orangered'}}/>}
		})
		return props.history.push(PAGE_PRODUCTS)
	}

	const back = () => {
		const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		recoverData.step = 3
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
		return props.history.push(PAGE_COMPLETE_PROFILE)
	}

	return <ComponentTicket order={order} resetOrder={resetOrder} nextStep={onFinish} btnLoading={btnLoading}
													getState={getState} history={props.history} back={back}/>
}

// Component for render print
export class ComponentTicket extends React.Component<any, any> {
	render() {
		const {order, resetOrder, nextStep, btnLoading, getState, history, back, backText} = this.props
		return <main style={{marginTop: 25, marginBottom: 25}}>
			<div className='container-body' style={{marginBottom: 75}}>
				<div style={{lineHeight: '16px', display: 'flex'}}>
					{order.order_id &&
          <img style={{marginRight: 20}} width={65} src={`${process.env.PUBLIC_URL}/192x192.png`} alt="Logo"/>}
					<div>
						<Title level={3} style={{marginBottom: 0}}>Confirmar información</Title>
						<Text style={{color: '#EB68CD'}}>Verifica que los datos de tu orden sean correctos antes de
							solicitarla</Text>
					</div>
				</div>
				<div style={{marginTop: 20}}>
					<Descriptions title="Información de la orden" layout="horizontal" column={2}>
						{order.order_id && <Descriptions.Item label={<strong>Orden</strong>}>{order.order_id}</Descriptions.Item>}
						{order.order_number > 0 &&
            <Descriptions.Item label={<strong>Numero de orden</strong>}>{order.order_number}</Descriptions.Item>}
						<Descriptions.Item label={<strong>Nombre</strong>}>{order.client_name}</Descriptions.Item>
						<Descriptions.Item label={<strong>Teléfono</strong>}>{order.client_telephone}</Descriptions.Item>
						<Descriptions.Item
							label={<strong>Fecha y hora de
								entrega</strong>}>{moment(order.delivery_date).format("dddd D, MMMM YYYY, h:mm a")}</Descriptions.Item>
						<Descriptions.Item label={<strong>Dirección</strong>}>{order.client_direction}</Descriptions.Item>
					</Descriptions>
				</div>

				<div>
					<Alert type='warning' message='El costo del envío se calcula en base a los lugares disponibles para las entregas, que puedes ver abajo' closable/>
				</div>

				<div style={{marginTop: 20}}>
					<Title level={3}>Productos</Title>
					<Table
						columns={columns}
						dataSource={order.products}
						pagination={false}
						footer={() => {
							return <div>
								<Text style={{fontSize: 'medium'}}><strong>Total: </strong>${order.total}</Text>
							</div>
						}}/>
				</div>

				{/*Delivery cost*/}
				<div className={styles.noPrint} style={{marginTop: 20, marginBottom: 50}}>
					<Title level={3}>Tabla de envíos</Title>
					<Table
						columns={columns2}
						dataSource={data}
						pagination={false}
						/>
				</div>

			</div>

			{/*Footer Buttons*/}
			<div className={styles.noPrint}>
				<FooterSteps
					history={history}
					back={<Button onClick={back} type='link' size='large'
												icon={<ArrowLeftOutlined/>}>{backText ? backText : 'Atras'}</Button>}
					refresh={getState}
					okText='Comprar'
					productsCard={order.products}
					countCant={order.countProduct}
					countProduct={order.products.length ? order.products.length : 0}
					total={order.total}
					resetOrder={resetOrder}
					functionNext={nextStep}
					btnLoading={btnLoading}
				/>
			</div>

		</main>
	}
}

const columns = [
	{
		title: 'Producto',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'Cantidad',
		dataIndex: 'quantity',
		key: 'quantity'
	},
	{
		title: 'Monto',
		dataIndex: 'amount',
		key: 'amount',
		render: (text: any) => <Text key='2'>${text}</Text>
	},
	{
		title: 'Precio total',
		dataIndex: 'amount',
		key: 'total',
		render: (text: any, row: any) => <Text key='1'>${text * row.quantity}</Text>
	}
]
const columns2 = [
	{
		title: 'Lugar',
		dataIndex: 'place',
		key: 'place',
	},
	{
		title: 'Costo de envío',
		dataIndex: 'delivery_cost',
		key: 'delivery_cost'
	}
]

const data = [
	{
		place: 'Chalatenango Centro',
		delivery_cost: 'GRATIS'
	},
	{
		place: 'Las Mercedes',
		delivery_cost: '$3,00'
	},
	{
		place: 'Azacualpa',
		delivery_cost: '$3,00'
	},
	{
		place: 'San Francisco Lempa',
		delivery_cost: '$3,00'
	},
	{
		place: 'San Luis del Carmen',
		delivery_cost: '$3,00'
	},
	{
		place: 'El Paraíso',
		delivery_cost: '$3,00'
	},
	{
		place: 'El Coyolito',
		delivery_cost: '$3,00'
	},
	{
		place: 'Santa Rita',
		delivery_cost: '$3,00'
	},
	{
		place: 'La Reina',
		delivery_cost: '$3,00'
	},
	{
		place: 'Agua caliente',
		delivery_cost: '$3,00'
	},
	{
		place: 'Nueva Concepción',
		delivery_cost: '$3,00'
	},
	{
		place: 'Concepción Quezaltepeque',
		delivery_cost: '$3,00'
	},
	{
		place: 'La Laguna',
		delivery_cost: '$3,00'
	},
	{
		place: 'Comalapa',
		delivery_cost: '$3,00'
	},
	{
		place: 'San Rafael',
		delivery_cost: '$3,00'
	},
	{
		place: 'Tejutla',
		delivery_cost: '$3,00'
	},
	{
		place: 'Los Ranchos',
		delivery_cost: '$3,00'
	},
	{
		place: 'Guarjila',
		delivery_cost: '$3,00'
	},
	{
		place: 'San Isidro',
		delivery_cost: '$3,00'
	},
	{
		place: 'Nombre de Jesús',
		delivery_cost: '$3,00'
	},
	{
		place: 'Arcatao',
		delivery_cost: '$3,00'
	},
	{
		place: 'Tepeyac',
		delivery_cost: '$2,00'
	},
	{
		place: 'Canyuco',
		delivery_cost: '$2,00'
	},
	{
		place: 'Las Mesas',
		delivery_cost: '$2,00'
	},
]

export default withRouter(ConfirmeOrder)