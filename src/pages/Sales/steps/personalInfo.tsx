import 'moment/locale/es'
import moment from 'moment';
import {withRouter} from 'react-router-dom'
import {FooterSteps} from "../../../components";
import React, {useEffect, useState} from "react";
import {ArrowLeftOutlined} from '@ant-design/icons'
import {PAGE_CONFIRME_ORDER, PAGE_PRODUCTS} from "../../../routes/";
import {Form, Input, Typography, Row, Col, DatePicker, Alert, Button, InputNumber} from 'antd';
import {delStorage, getStorage, setStorage, verifyStorage} from "../../../services/Storage";

const {Title, Text} = Typography

const PersonalInfo: React.FC<any> = (props) => {
	const [form] = Form.useForm()
	const [productsCard, setProductsCard] = useState<any>([])
	const [orderCount, setOrderCount] = useState({total: 0, countProduct: 0})
	const [btnLoading, setBtnLoading] = useState(false)

	useEffect(() => {
		getState()
		// eslint-disable-next-line
	}, [])

	const getState = () => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			if (verify) {
				const data = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
				const {total, countProduct, products} = data
				if (data.step === 3) {
					setOrderCount({total, countProduct})
					data.delivery_date = data.delivery_date ? moment(data.delivery_date) : ''
					form.setFieldsValue(data)
					return setProductsCard(products)
				} else {
					return props.history.push(PAGE_PRODUCTS)
				}
			}
		}
		return props.history.push(`${PAGE_PRODUCTS}`)
	}

	function range(start: any, end: any) {
		const result = [];
		for (let i = start; i < end; i++) {
			result.push(i);
		}
		return result;
	}

	function disabledDateTime() {
		return {
			disabledHours: () => {
				const val = range(0, 24).splice(0, 8)
				const val2 = range(0, 24).splice(19, 24)
				return [...val, ...val2]
			}
		};
	}

	function disabledDate(current: any) {
		return current && current < moment(new Date()).add(-1, 'days');
	}

	const resetOrder = async () => {
		const del = delStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (del) {
			return props.history.push(PAGE_PRODUCTS)
		}
	}

	const onFinish = async (values: any) => {
		setBtnLoading(true)
		const getData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		getData.delivery_date = values.delivery_date.toISOString()
		getData.step = 4
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, getData)
		return props.history.push(PAGE_CONFIRME_ORDER)
	}

	const nextStep = async () => {
		form.submit()
	}

	const handleChange = (event: any) => {
		event.persist()
		const {name, value} = event.target
		const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		recoverData[name] = value
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
	}

	const handleChangeNumber = (event: any) => {
		const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		recoverData.client_telephone = event
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
	}

	const dateChange = (event: any) => {
		const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		recoverData.delivery_date = event.toISOString()
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
	}

	const back = () => {
		const recoverData = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		recoverData.step = 2
		setStorage(`${process.env.REACT_APP_ORDER_NAME}`, recoverData)
		return props.history.push(`${PAGE_PRODUCTS}/2`)
	}

	return <main style={{marginTop: 25, marginBottom: 25}}>
		<div className='container-form' style={{marginBottom: 75}}>
			<Alert
				closable
				message="Recuerda que debes de ingresar tú número de teléfono real al cual podamos llamar para confirmar tu
				orden, además de nombre completo, hora, fecha de entrega y una descripción detallada de tu ubicación."
				type="warning"/>
			<br/>
			<div style={{lineHeight: '16px', marginBottom: 15}}>
				<Title level={3} style={{marginBottom: 0}}>Información personal</Title>
				<Text><strong>La información compartida es unidamente para el uso de de la orden</strong></Text>
			</div>

			<Form form={form} name="add-personal-info" onFinish={onFinish}>
				<Row justify='center' gutter={[12, 12]}>

					<Col sm={24} md={12}>
						<Form.Item name="client_name" labelCol={{span: 24, offset: 24}} labelAlign='left' label="Nombre completo"
											 rules={[{required: true, message: 'Campo obligatorio'}]}>
							<Input name="client_name" onChange={handleChange} placeholder='Ejemplo: Carlos Vides Guardado'/>
						</Form.Item>
					</Col>

					<Col sm={24} md={12}>
						<Form.Item name="client_telephone" labelCol={{span: 24, offset: 24}} labelAlign='left'
											 label="Numero telefónico"
											 rules={[{required: true, message: 'Campo obligatorio'}]}>
							<InputNumber style={{width: '100%'}} name="client_telephone" onChange={handleChangeNumber}
													 placeholder="Ejemplo: 23010045"/>
						</Form.Item>
					</Col>

					<Col sm={24} md={12}>
						<Form.Item name="client_email" labelCol={{span: 24, offset: 24}} labelAlign='left'
											 label="Correo electrónico"
											 rules={[{required: true}, {type: 'email', message: 'Ingresé un correo válido'}]}>
							<Input name="client_email" onChange={handleChange} placeholder="Ejemplo: exmaple@mail.com"/>
						</Form.Item>
					</Col>

					<Col sm={24} md={12}>
						<Form.Item name="delivery_date" labelCol={{span: 24, offset: 24}} labelAlign='left'
											 label="Fecha y hora para el envío"
											 rules={[{required: true, message: 'Campo obligatorio'}]}>
							<DatePicker
								onChange={dateChange}
								style={{width: '100%'}}
								format="YYYY-MM-DD HH:mm"
								disabledDate={disabledDate}
								disabledTime={disabledDateTime}
								showTime={{defaultValue: moment('00:00:00', 'HH:mm:ss')}}
							/>
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item name="client_direction" labelCol={{span: 24, offset: 24}} labelAlign='left'
											 label="Dirección de envío"
											 rules={[{required: true, message: 'Campo obligatorio'}]}>
							<Input.TextArea placeholder="Frente a parque central, casa #56" name="client_direction"
															autoSize={{minRows: 3}} onChange={handleChange}/>
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item
							initialValue=''
							name="client_comment" labelCol={{span: 24, offset: 24}}
							labelAlign='left'
							label="Comentarios adicionales (opcional)">
							<Input.TextArea placeholder="Ejemplo: Que el pastel venga bien empaquetado" name="client_comment"
															autoSize={{minRows: 4}} onChange={handleChange}/>
						</Form.Item>
					</Col>
				</Row>
			</Form>
		</div>

		{/*Footer Buttons*/}
		<FooterSteps
			history={props.history}
			refresh={getState}
			back={<Button onClick={back} type='link' size='large'
										icon={<ArrowLeftOutlined/>}>{props.backText ? props.backText : 'Atras'}</Button>}
			productsCard={productsCard}
			countCant={orderCount.countProduct}
			countProduct={productsCard.length ? productsCard.length : 0}
			total={orderCount.total}
			resetOrder={resetOrder}
			functionNext={nextStep}
			btnLoading={btnLoading}
		/>
	</main>
}

export default withRouter(PersonalInfo)