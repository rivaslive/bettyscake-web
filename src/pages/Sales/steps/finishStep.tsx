import {Order} from "../../../models";
import {Typography, Button} from 'antd'
import ReactToPrint from "react-to-print";
import {withRouter} from 'react-router-dom'
import {BgLayout} from "../../../components";
import {ComponentTicket} from "./confirmeOrder";
import React, {useEffect, useState} from "react";
import {CELLPHONE_NUMBER} from "../../../config";
import {FilePdfOutlined} from '@ant-design/icons'
import sendNotification from "../../../components/Notifications";
import {delStorage, getStorage, verifyStorage} from "../../../services/Storage";
import {PAGE_COMPLETE_PROFILE, PAGE_PRODUCTS} from "../../../routes/";

const {Title} = Typography

const FinishStep: React.FC<any> = (props) => {
	const componentRef = React.useRef<any>();
	const [order, setOrder] = useState<Order>({
		delivery_date: "",
		client_name: "",
		client_direction: "",
		client_telephone: "",
		products: [],
		client_comment: "",
		client_email: "",
		total: 0,
		countProduct: 0,
		step: 5
	})

	useEffect(() => {
		getState()
		// eslint-disable-next-line
	}, [])
	const getState = () => {
		const verify = verifyStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (verify) {
			const data = getStorage(`${process.env.REACT_APP_ORDER_NAME}`)
			const {step} = data
			if (step === 1 || step === 2) {
				return props.history.push(`${PAGE_PRODUCTS}`)
			} else if (step === 3) {
				return props.history.push(`${PAGE_COMPLETE_PROFILE}`)
			} else {
				// Esta seria nuestra PAGE
				return setOrder(data)
			}
		}
		return props.history.push(`${PAGE_PRODUCTS}`)
	}

	const newOrder = () => {
		const del = delStorage(`${process.env.REACT_APP_ORDER_NAME}`)
		if (del) return props.history.push(PAGE_PRODUCTS)
		return sendNotification({
			type: 'warning',
			message: 'Algo salio mal',
			description: `No hemos podido procesar tu orden, puedes obtener más información llamando al ${CELLPHONE_NUMBER}`
		})
	}

	return <BgLayout cancelTopBar={true}>
		<div style={{marginTop: -75}}>
			<div
				style={{
					marginLeft: 'auto',
					marginRight: 'auto',
					width: 100,
					height: 100,
					marginBottom: 35,
					backgroundImage: `url("${process.env.PUBLIC_URL}/192x192.png")`,
					backgroundSize: 'cover'
				}}/>
			<Title level={3} style={{textAlign: 'center'}}>¡Felicidades hemos recibido tu orden con éxito!</Title>
			<br/>
			<ReactToPrint
				trigger={() => <Button type='primary' style={{width: '100%'}} icon={<FilePdfOutlined/>}>Descargar comprobante de
					compra</Button>}
				content={() => componentRef.current}
			/>
			<div style={{display: "none"}}>
				<ComponentTicket ref={componentRef} order={order}/>
			</div>
			<div style={{marginTop: 20, width: '100%'}}>
				<Button type='default' htmlType='button' onClick={newOrder}
								style={{width: '100%', background: 'rgb(138, 179, 30)', color: 'white'}}>Nueva compra</Button>

			</div>
		</div>
	</BgLayout>
}

export default withRouter(FinishStep)
