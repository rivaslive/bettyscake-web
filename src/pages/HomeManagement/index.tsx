import styles from './style.module.css'
import {CardOrder} from '../../components'
import {withRouter} from 'react-router-dom'
import {serverRequest} from "../../services";
import {ManagementOrder} from "../../models";
import React, {useEffect, useState} from 'react'
import {Menu, Row, Col, Typography, Input, Pagination} from 'antd'
import {
	PAGE_ADMIN_DETAILS_ORDER,
	PAGE_AUTH_LOGIN,
	URL_MANAGEMENT_LIST_ORDERS,
	URL_MANAGEMENT_SEARCH_ORDER
} from "../../routes/";
import {
	CheckCircleOutlined,
	CloseCircleOutlined,
	RollbackOutlined,
	SearchOutlined,
	LoadingOutlined,
	LogoutOutlined
} from '@ant-design/icons'
import {delStorage} from "../../services/Storage";

const {Title, Text} = Typography

interface InternalPagination {
	page: number
	total_items: number
	total_pages: number
}

function getWindowDimensions() {
	const {innerWidth: width} = window;
	return {width}
}

function useWindowDimensions() {
	const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

	useEffect(() => {
		function handleResize() {
			setWindowDimensions(getWindowDimensions());
		}

		window.addEventListener('resize', handleResize);
		return () => window.removeEventListener('resize', handleResize);
	}, []);

	return windowDimensions;
}

// COMPONENT FUNCTION
const HomeManagement: React.FC<any> = (props) => {
	const [orders, setOrder] = useState<ManagementOrder[]>([])
	const [pagination, setPagination] = useState<InternalPagination>({page: 1, total_items: 0, total_pages: 0})
	const [page, setPage] = useState<number>(1)
	const [filter, setFilter] = useState<any>({key: '', value: ''})
	const [loading, setLoading] = useState<boolean>(true)
	const {width}: any = useWindowDimensions();

	useEffect(() => {
		getData(1).catch(() => {
		})
		// eslint-disable-next-line
	}, [])

	useEffect(() => {
		if (page !== pagination.page) {
			getData(page).catch(() => {
			})
		}
		// eslint-disable-next-line
	}, [page])

	const getData = async (page2: number, filterer = {key: '', value: ''}) => {
		const getOrders = await serverRequest({action: URL_MANAGEMENT_LIST_ORDERS, page: page2, filter: filterer.key ? filterer : filter})
		console.log(getOrders);
		if (getOrders) {
			setOrder(getOrders.items)
			setPagination(getOrders.pagination)
		}
		setLoading(false)
	}

	const handleClick = (e: any) => {
		setPage(1)
		switch (e.key) {
			case '1':
				setFilter({key: '', value: ''})
				return getData(1, {key: 'nothing', value: ''})
			case '2':
				setFilter({key: 'status', value: 'IN_PROCESS'})
				return getData(1,{key: 'status', value: 'IN_PROCESS'})
			case '3':
				setFilter({key: 'status', value: 'PENDING'})
				return getData(1, {key: 'status', value: 'PENDING'})
			case '4':
				setFilter({key: 'status', value: 'ARCHIVED'})
				return getData(1, {key: 'status', value: 'ARCHIVED'})
			case '5':
				return logout()
			default:
				setFilter({key: '', value: ''})
				return getData(1, {key: '', value: ''})
		}
	};

	const searchOrder = async (value: any) => {
		value.persist()
		setPage(1)
		setFilter({key: '', value: ''})
		setLoading(true)
		const getOrders = await serverRequest({
			action: URL_MANAGEMENT_SEARCH_ORDER,
			q: value.target.value
		})
		console.log(getOrders);
		if (getOrders) {
			setOrder(getOrders.items)
			setPagination(getOrders.pagination)
		}
		setLoading(false)
	}

	const detailOrder = (order_id: string) => {
		return props.history.push(`${PAGE_ADMIN_DETAILS_ORDER}/${order_id}`)
	}

	const logout = () => {
		delStorage('auth')
		props.history.push(PAGE_AUTH_LOGIN)
	}


	return <div>
		<Row>
			<Col lg={6} md={8} sm={24} xs={24} style={{padding: 30}}>
				<div className={styles.header}>
					<img src={`${process.env.PUBLIC_URL}/192x192.png`} width={75} alt="Logo"/>
				</div>
				<Menu
					onClick={handleClick}
					style={width < 768 ? {width: '100%'} : {width: 200, maxWidth: '100%'}}
					defaultSelectedKeys={['1']}
					defaultOpenKeys={['sub1']}
					mode={width > 767 ? 'vertical' : 'horizontal'}
				>
					<Menu.Item key="1" icon={<CheckCircleOutlined/>}>Todas las ordenes</Menu.Item>
					<Menu.Item key="3" icon={<CloseCircleOutlined/>}>Ordenes sin confirmar</Menu.Item>
					<Menu.Item key="2" icon={<CheckCircleOutlined/>}>Ordenes en proceso</Menu.Item>
					<Menu.Item key="4" icon={<RollbackOutlined/>}>Ordenes pasadas</Menu.Item>
					<Menu.Item key="5" style={{background: '#EB68CD', color: 'white', marginTop: 50}}
										 icon={<LogoutOutlined/>}>Cerrar sesión</Menu.Item>
				</Menu>
			</Col>
			<Col sm={24} md={16} lg={18} xs={24} style={{minHeight: '100vh'}}>
				<div style={{width: 820, maxWidth: '100%', height: '100%', padding: 20}}>
					<Title level={3} style={width < 768 ? {marginTop: 0} : {marginTop: 35}}>Panel Administrativo</Title>
					<div>
						<Input onChange={searchOrder} placeholder='Buscar una orden' prefix={<SearchOutlined style={{color: 'grey'}}/>}/>
					</div>
					<div style={{marginTop: 30}}>
						<Text><strong>Ordenes</strong></Text>
						<div style={{marginTop: 10}}>
							{
								loading ?
									<div style={{width: '100%', textAlign: 'center', marginTop: 50}}><LoadingOutlined
										style={{fontSize: 40, color: '#EB68CD'}}/></div>
									:
									<React.Fragment>
										<Row justify={width < 1086 ? 'center' : 'start'} gutter={[12, 12]} style={{marginBottom: 50}}>
											{
												orders.map((ord, i) => {
													return <Col key={i}>
														<CardOrder order={ord} detailOrder={detailOrder}/>
													</Col>
												})
											}
										</Row>
										<div style={{
											position: 'fixed',
											background: 'white',
											width: '100%',
											padding: 15,
											bottom: 0,
											textAlign: 'center',
											left: 0
										}}>
											<Pagination
												current={page}
												style={{width: '100%'}}
												size="default"
												showQuickJumper
												total={pagination.total_items}
												pageSize={20}
												onChange={(e) => setPage(e)}/>
										</div>
									</React.Fragment>
							}
						</div>
					</div>
				</div>
			</Col>
		</Row>
	</div>
}

export default withRouter(HomeManagement)

