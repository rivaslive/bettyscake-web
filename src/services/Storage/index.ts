import CryptoJS from 'crypto-js'
import {Order} from "../../models";

// Almacena una clave en el LocalStorage
export function setStorage(key: string, value: any) {
	if (localStorage.getItem(key) !== null) localStorage.removeItem(key)
	const data = CryptoJS.AES.encrypt(JSON.stringify(value), `${process.env.REACT_APP_ENCRYPT_KEY}`).toString();
	localStorage.setItem(key, data)
}

// Obtiene una clave del almacenamiento en LocalStorage
export function getStorage(key: string) {
	if (localStorage.getItem(key) !== null) {
		const data: any = localStorage.getItem(key)
		const bytes  = CryptoJS.AES.decrypt(data, `${process.env.REACT_APP_ENCRYPT_KEY}`).toString(CryptoJS.enc.Utf8);
		return JSON.parse(bytes)
	}
}

// Verifica si existe una clave en el almacenamiento de LocalStorage
export function verifyStorage(key: string) {
	return localStorage.getItem(key) !== null;
}

export function delStorage(key: string) {
	if (verifyStorage(key)) {
		localStorage.removeItem(key)
		return true
	} return false
}

export const initialOrder: Order = {
	delivery_date: "",
	client_name: "",
	client_direction: "",
	client_telephone: "",
	products: [],
	client_comment: "",
	client_email: "",
	total: 0,
	countProduct: 0,
	step: 1
}
