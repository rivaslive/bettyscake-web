import apiInstance, {axiosInternal} from '../../config'
import {
	URL_GET_LIST_PRODUCTS,
	PRODUCTS,
	URL_GET_LIST_MISCS,
	MISCS,
	URL_POST_ORDER,
	ORDERS,
	URL_AUTH_LOGIN,
	AUTH_LOGIN,
	URL_MANAGEMENT_LIST_ORDERS,
	PAGE_AUTH_LOGIN,
	URL_MANAGEMENT_GET_BY_ID_ORDER,
	URL_MANAGEMENT_CHANGE_STATUS_ORDER,
	CHANGE_STATUS_ORDER,
	URL_MANAGEMENT_DELETE_ORDER,
	URL_MANAGEMENT_SEARCH_ORDER,
	URL_MANAGEMENT_UPDATE_ORDER
} from '../../routes/'
import {getStorage, verifyStorage} from "../Storage";

interface InternalProps {
	action: string
	body?: any
	id?: string
	page?: number
	q?: string
	filter?: {
		key: string
		value: string
	}
}

const serverRequest = async ({action, body = {}, id = '', filter= {key: '', value: ''}, page=1, q=''}: InternalProps) => {
	const token = () => {
		if (verifyStorage('auth')) {
			const {token} = getStorage('auth')
			return `Bearer ${token}`
		}
		return window.location.assign(PAGE_AUTH_LOGIN)
	}

	switch (action) {
		case URL_GET_LIST_PRODUCTS:
			try {

				const {data} = await apiInstance(PRODUCTS)
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data.message
				console.log(err);
				return false
			}

		case URL_GET_LIST_MISCS:
			try {
				const {data} = await apiInstance(`${PRODUCTS}/${MISCS}`)
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data.message
				console.log(err);
				return false
			}

		case URL_POST_ORDER:
			try {
				const {data} = await apiInstance.post(`${ORDERS}`, body)
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		// MANAGEMENT
		case URL_AUTH_LOGIN:
			try {
				const {data} = await axiosInternal.post(AUTH_LOGIN, body)
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		case URL_MANAGEMENT_UPDATE_ORDER:
			try {
				const {data} = await axiosInternal.put(`${ORDERS}/${id}`, body, {
					headers: {
						Authorization: token()
					}
				})
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		case URL_MANAGEMENT_LIST_ORDERS:
			try {
				const {data} = await axiosInternal(`${ORDERS}/?page=${page}&filter[${filter?.key}]=${filter.value}`, {
					headers: {
						Authorization: token()
					}
				})
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		case URL_MANAGEMENT_SEARCH_ORDER:
			try {
				console.log(`${ORDERS}/?search=${q}`)
				const {data} = await axiosInternal(`${ORDERS}/?search=${q}`, {
					headers: {
						Authorization: token()
					}
				})
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		case URL_MANAGEMENT_GET_BY_ID_ORDER:
			try {
				const {data} = await axiosInternal(`${ORDERS}/${id}`, {
					headers: {
						Authorization: token()
					}
				})
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		case URL_MANAGEMENT_CHANGE_STATUS_ORDER:
			try {
				const {data} = await axiosInternal.post(`${ORDERS}/${CHANGE_STATUS_ORDER}`, body, {headers: {Authorization: token()}})
				return data
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		case URL_MANAGEMENT_DELETE_ORDER:
			try {
				await axiosInternal.delete(`${ORDERS}/${id}`, {headers: {Authorization: token()}})
				return true
			} catch (e) {
				let err: any = {message: 'Error desconocido'}
				err = e.response !== undefined && e.response.data
				console.log(err);
				return false
			}

		default:
			break;
	}
}

export default serverRequest