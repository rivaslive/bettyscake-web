export interface ShowModalAddProductInterface {
	product_id: string
	images: string[]
	name: string
	description: string
	options: {
		option_id: string
		name: string
		amount: number
	}[]
}

export interface OrderProduct {
	product_id: string
	name: string
	image: string
	option_id: string
	amount: number
	quantity: number
	dedications: string[]
}

export interface Order {
	order_id?: string
	order_number?: number
	delivery_date: string
	client_name: string
	client_direction: string
	client_telephone: string
	products: OrderProduct[]
	client_comment: string
	client_email: string
	total: number
	countProduct: number
	step: number
}


export interface ManagementOrderProduct {
	amount: number
	category: string
	dedications: string[]
	images: string[]
	name: string
	option: {option_id: string, option_name: string}
	product_id: string
	quantity: number
}

export interface ManagementOrder {
	order_id: string
	order_number: number
	delivery_date: string
	delivery_date_text: string
	client_name: string
	client_direction: string
	client_telephone: string
	cost_delivery: number
	products: ManagementOrderProduct[]
	client_comment: string
	client_email: string
	order_total: number
	payment: number
	remaning_payment: number
	status: string
	status_text: string
}
