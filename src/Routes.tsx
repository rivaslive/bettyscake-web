import React from 'react';
import {AuthPage, ConfirmeOrder, FinishStep, HomePage, OneStep, PersonalInfo, HomeManagement, DetailsManagement} from './pages'
import {LayoutComponent} from "./components";
import {
	PAGE_ADMIN_DETAILS_ORDER,
	PAGE_ADMIN_HOME,
	PAGE_AUTH_LOGIN,
	PAGE_COMPLETE_PROFILE,
	PAGE_CONFIRME_ORDER,
	PAGE_FINISH_ORDER, PAGE_HOME,
	PAGE_PRODUCTS
} from './routes/';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import {verifyStorage} from "./services/Storage";

function PrivateRoute({children, ...rest}: any) {
	let isAuthenticated = false
	const verify = verifyStorage('auth')
	if (verify) {
		isAuthenticated = true
	}
	return (
		<Route
			{...rest}
			render={
				({location}) =>
					!isAuthenticated ?
						<Redirect to={{pathname: "/auth/login", state: {from: location}}}/> : children
			}
		/>
	);
}

function Routes(props: any) {
	return <BrowserRouter>
		<Switch>
			{/*Public URLS*/}
			<Route path="/loaderio-f3edc433314d8957d98a1b15d54c6fb3" exact>
				loaderio-f3edc433314d8957d98a1b15d54c6fb3
			</Route>

			<Route path={PAGE_HOME} exact>
				<LayoutComponent>
					<HomePage/>
				</LayoutComponent>
			</Route>
			{/*Steps*/}
			<Route path={PAGE_PRODUCTS} exact>
				<LayoutComponent>
					<OneStep/>
				</LayoutComponent>
			</Route>

			{/*Steps*/}
			<Route path={PAGE_COMPLETE_PROFILE} exact>
				<LayoutComponent>
					<PersonalInfo/>
				</LayoutComponent>
			</Route>

			<Route path={PAGE_CONFIRME_ORDER} exact>
				<LayoutComponent>
					<ConfirmeOrder/>
				</LayoutComponent>
			</Route>

			<Route path={PAGE_FINISH_ORDER} exact>
				<LayoutComponent>
					<FinishStep/>
				</LayoutComponent>
			</Route>

			<Route path={`${PAGE_PRODUCTS}/:step`} exact>
				<LayoutComponent>
					<OneStep/>
				</LayoutComponent>
			</Route>

			{/*Login*/}
			<Route path={PAGE_AUTH_LOGIN} exact>
				<LayoutComponent>
					<AuthPage/>
				</LayoutComponent>
			</Route>

			{/*Private URL*/}
			<PrivateRoute path={PAGE_ADMIN_HOME} exact>
				<HomeManagement/>
			</PrivateRoute>

			<PrivateRoute path={`${PAGE_ADMIN_DETAILS_ORDER}/:order_id`} exact>
				<DetailsManagement/>
			</PrivateRoute>

			{/*Index*/}
			{
				props.isAdmin ?
					<PrivateRoute>
							<HomeManagement/>
					</PrivateRoute>
					:
					<Route>
						<LayoutComponent>
							<HomePage/>
						</LayoutComponent>
					</Route>
			}
		</Switch>
	</BrowserRouter>
}

export default Routes;
