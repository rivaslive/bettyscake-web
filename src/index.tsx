import './index.less';
import React, {useEffect, useState} from 'react';
import 'antd/dist/antd.less';
import Routes from './Routes';
import ReactDOM from 'react-dom';
import {ConfigProvider} from "antd";
import es_ES from 'antd/es/locale/es_ES'
import * as serviceWorker from './serviceWorker';
import {verifyStorage} from "./services/Storage";

const RenderRouter = () => {
	const [isAdmin, setIsAdmin] = useState(false)

	useEffect(() => {
		const verify = verifyStorage('auth')
		if (verify) {
			setIsAdmin(true)
		}
	}, [])

	return <Routes isAdmin={isAdmin}/>
}

ReactDOM.render(
	<ConfigProvider locale={es_ES}>
		<RenderRouter/>
	</ConfigProvider>,
	document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
