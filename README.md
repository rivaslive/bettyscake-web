# bettyscake - web

Web app for Betty's Cake


#Estructura

```bash
.
├── public
│   ├── robots.txt
│   ├── manifest.json
│   ├── index.html
│   └── favicon.ico
├── src
│   ├── Routes.tsx
│   ├── index.ts
│   ├── index.css
│   ├── pages.ts
│   ├── services
│   │   └── index.ts
│   ├── routes
│   │   └── index.ts
│   ├── pages
│   │   └── index.ts
│   ├── models
│   │   └── index.ts
│   └── components
│       └── index.ts
├── package.json
└── README.md
```