const CracoLessPlugin = require('craco-less');
process.env.BROWSER = "none";
module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: {
                            '@primary-color': '#EB68CD',
                            '@link-color': '#EB68CD',
                            '@border-radius-base': '6px', // major border radius
                            '@skeleton-color': '#fff5f9',
                            '@input-bg': '#FFE8FA',
                            '@picker-bg': '@input-bg',
                            '@menu-item-active-bg': '#8ab31e',
                            '@menu-highlight-color': '@menu-item-active-bg',
                        },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
};